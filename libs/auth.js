const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const config = require('../config');

async function genSalt(saltRounds) {
  return bcrypt.genSalt(saltRounds);
}

async function hashPassword(password, salt) {
  return bcrypt.hash(password, salt);
}

async function genPassword(password) {
  const salt = await genSalt(config.get('default_salt_rounds'));
  return hashPassword(password, salt);
}

const sign = ({ _id }) => {
  const token = jwt.sign({ id: _id }, config.get('secret'));
  return token;
}

async function verify(token) {
  try {
    return jwt.verify(token, config.get('secret'));
  } catch(err) {
    console.log('Error: Validation of token', err);
    return false;
  }
}

async function checkPassword(password, hash) {
  return bcrypt.compare(password, hash);
}

module.exports = {
  genSalt,
  hashPassword,
  genPassword,
  sign,
  verify,
  checkPassword,
};
