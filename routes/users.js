const express = require('express');
const router = express.Router();

const config = require('../config');

const { User } = require('../schemas');
const { genPassword, sign, verify, checkPassword } = require('../libs/auth');

router.get('/', async (req, res) => {
  const {
    idUser,
    username,
    email,
    phoneNumber,
  } = req.query;

  const token = req.headers['x-access-token'];

  const decoded = await verify(token);

  const params = {
    _id: decoded.id || idUser,
    username,
    email,
    phoneNumber,
  };

  Object.keys(params).forEach(key => params[key] === undefined ? delete params[key] : '');

  User.findOne(params, (err, user) => {
    if (err) {
      console.error('Internal Server Error: [user] (get): ', err);
      return res.status(500).send(config.get('http_error_messages')['500']);
    }

    if (!user) {
      console.error('User not found');
      return res.status(404).send(config.get('http_error_messages')['404']);
    }

    const userWithoutPass = { ...user.toObject() };
    delete userWithoutPass.password;
    delete userWithoutPass['__v'];

    res.status(200).json(userWithoutPass);
  });
});

router.post('/', async (req, res) => {
  const { body } = req;
  const hashedPassword = await genPassword(body.password);

  User.findOne({ email: body.email, username: body.username }, async (err, user) => {
    if (err) {
      console.error('Internal Server Error: [user]', err);
      return res.status(500).send(config.get('http_error_messages')['500']);
    }

    if (user) {
      return res.status(409).send(config.get('http_error_messages')['409']);
    }

    User.create({ ...body, password: hashedPassword }, async (err, user) => {
      if (err) {
        console.error('Internal Server Error: [user]: ', err);
        return res.status(500).send(config.get('http_error_messages')['500']);
      }

      const token = await sign(user);

      res.status(200).json({ token });
    })

  });
});

router.post('/changePassword', async (req, res) => {
  const { token, oldPassword, newPassword, type } = req.body;

  const decoded = await verify(token);

  User.findOne({ _id: decoded.id }, async (err, user) => {
    if (err) {
      console.error('Internal Server Error: [user/changePassword]: ', err);
      return res.status(500).send(config.get('http_error_messages')['500']);
    }

    if (!user) {
      console.error('User not found');
      return res.status(404).send(config.get('http_error_messages')['404']);
    }

    const passwordType = type == 0 ? 'password' : 'finPassword';

    const passIsValid = await checkPassword(oldPassword, user[passwordType]);
    if (!passIsValid) {
      return res.status(401).send(config.get('http_error_messages')['401']);
    }

    const hashedPassword = await genPassword(user[passwordType]);

    User.findOneAndUpdate({ _id: user._id }, { [passwordType]: hashedPassword }, (err) => {
      if (err) {
        console.error('Internal Server Error: [user/changePassword]: ', err);
        return res.status(500).send(config.get('http_error_messages')['500']);
      }

      res.status(200).send('Password changed!');
    })
  });
});

module.exports = router;
