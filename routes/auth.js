const express = require('express');
const router = express.Router();

const config = require('../config');

const { User } = require('../schemas');

const { checkPassword, sign } = require('../libs/auth');

router.get('/', (req, res) => {
  const { username, password } = req.query;

  User.findOne({ username }, async (err, user) => {
    if (err) {
      console.error('Error: Auth login: ', err);
      return res.status(500).send(config.get('http_error_messages')['500']);
    }

    if (!user) {
      console.error(`${username} not found`);
      return res.status(404).send(config.get('http_error_messages')['404']);
    }

    const passIsValid = await checkPassword(password, user.password);
    if (!passIsValid) {
      return res.status(401).send(config.get('http_error_messages')['401']);
    }

    const token = await sign({ _id: user._id });
    res.status(200).json({ token });
  });
});

module.exports = router;
