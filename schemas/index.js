const mongoose = require('mongoose');

const user = {
  sponsor: {
    type: String,
    // required: true,
  },
  username: {
    type: String,
    unique: true,
    // required: true,
  },
  email: {
    type: String,
    unique: true,
    // required: true,
  },
  firstName: {
    type: String,
    // required: true,
  },
  secondName: {
    type: String,
    // required: true,
  },
  phone: {
    type: Number,
    // required: true,
  },
  country: {
    type: String,
    // required: true,
    default: 'ru',
  },
  password: {
    type: String,
    // required: true,
  },
  finPassword: {
    type: String,
    // required: true,
  },
  skype: {
    type: String,
  },

  cell_number: {
    type: String,
    // required: true,
    validate: {
      validator: function(v) {
        return /\d{3}-\d{3}-\d{4}/.test(v);
      },
      message: '{VALUE} is not a valid phone number!'
    },
    // required: [true, 'User phone number required']
  },
};

const UserSchema = new mongoose.Schema(user);
const User = mongoose.model('User', UserSchema);

module.exports = {
  User,
};

// sponsor: {type: String} — логин пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (). «-»,«» не могут находиться в начале и конце);
// username: {type: String} — логин пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (). «-»,«» не могут находиться в начале и конце);
// email: {type: String} — email пользователя (допустимы латинские буквы (a-z), цифры (0-9), дефис (-) и нижнее подчеркивание (). «-»,«» не могут находиться в начале и конце);
// firstName: {type: String} — имя пользователя (допустимы любые буквы, цифры (0-9));
// secondName: {type: String} — фамилия пользователя (допустимы любые буквы, цифры (0-9));
// phone: {type: Number} — номер телефона пользователя (допустимы цифры (0-9), номер телефона должен быть в полном формате);
// country: {type: String} – код страны пользователя в двухбуквенном формате ISO, по умолчанию «ru»;
// password: {type: String} — основной пароль (допустимы любые символы);
// finPassword: {type: String} — финансовый основной пароль (допустимы любые символы).
